package ru.kpfu.itis.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.kpfu.itis.pages.ConfProperties;
import ru.kpfu.itis.pages.LoginPage;
import ru.kpfu.itis.pages.PageProfile;

import java.util.concurrent.TimeUnit;

public class Test0_1 {
    public static LoginPage loginPage;
    public static PageProfile pageProfile;
    public static WebDriver driver;
    private static final long PAGE_LOADING_WAIT_TIMEOUT = TimeUnit.SECONDS.toMillis(35);

//    public void waitForPageLoading() {
//        try {
////            driver.findElement(By.ByXPath())
//            driver.findElement(By.xpath("//*[contains(text(), 'Войти')]")).wait(PAGE_LOADING_WAIT_TIMEOUT).click();
//        } catch (AssertionError | InterruptedException e) {
//            throw new AssertionError(
//                    String.format("Page wasn't loaded within %d seconds",
//                            TimeUnit.MILLISECONDS.toSeconds(PAGE_LOADING_WAIT_TIMEOUT)),
//                    e);
//        }
//    }

    public static void main(String[] args) {
    }
    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://passport.yandex.ru/auth");

        driver.get(ConfProperties.getProperty("loginpage"));

        loginPage = new LoginPage(driver);
        pageProfile = new PageProfile(driver);
    }

    @Test
    public void test_0_1_1and2and3and4() throws InterruptedException {
        loginPage.inputLogin(ConfProperties.getProperty("login"));
        driver.findElement(By.xpath("//*[contains(@class, 'sc-pNWxx sc-jrsJCI dryRrI emsrNO')]")).click();
        driver.findElement(By.xpath("//button[contains(@class, 'Button2 Button2_size_l Button2_view_action Button2_width_max Button2_type_submit')]")).click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.findElement(By.id("passp-field-passwd")).sendKeys(ConfProperties.getProperty("password"));
        driver.findElement(By.xpath("//button[contains(@class, 'Button2 Button2_size_l Button2_view_action Button2_width_max Button2_type_submit')]")).click();

        Assert.assertEquals(ConfProperties.getProperty("name"), driver.findElement(By.xpath("//span[contains(@class, 'user-account__name')]")).getText());
        Assert.assertEquals(ConfProperties.getProperty("personal-info__first"),
                driver.findElement(By.xpath("//div[contains(@class, 'personal-info__first')]")).getText());
        Assert.assertEquals(ConfProperties.getProperty("personal-info__last"),
                driver.findElement(By.xpath("//div[contains(@class, 'personal-info__last')]")).getText());
        String user = pageProfile.getUserName();
        Assert.assertEquals(ConfProperties.getProperty("name"), user);

    }

    @After
    public void tearDown() {
        pageProfile.entryMenu();
        pageProfile.userLogout();
        driver.quit();

    }
}
