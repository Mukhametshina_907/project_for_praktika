package ru.kpfu.itis.tests;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.kpfu.itis.pages.ConfProperties;
import ru.kpfu.itis.pages.LoginPage;
import ru.kpfu.itis.pages.PageProfile;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class Test1_4 {
    public static WebDriver driver;
    public static LoginPage loginPage;
    public static PageProfile pageProfile;

    @BeforeClass
    public static void setup() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(ConfProperties.getProperty("loginpage"));
        loginPage = new LoginPage(driver);
        pageProfile = new PageProfile(driver);
        login();
    }

    public static void login() throws InterruptedException {
        driver.get(ConfProperties.getProperty("loginpage"));

        loginPage.inputLogin(ConfProperties.getProperty("login"));
//        driver.findElement(By.xpath("//*[contains(@class, 'sc-pNWxx sc-jrsJCI dryRrI emsrNO')]")).click();
        driver.findElement(By.xpath("//button[contains(@class, 'Button2 Button2_size_l Button2_view_action Button2_width_max Button2_type_submit')]")).click();
        driver.findElement(By.id("passp-field-passwd")).sendKeys(ConfProperties.getProperty("password"));
//        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        Thread.sleep(12000);
        driver.findElement(By.xpath("//button[contains(@class, 'Button2 Button2_size_l Button2_view_action Button2_width_max Button2_type_submit')]")).click();
//        String user = pageProfile.getUserName();

    }


    @Test
    public void afishaTest1_5_1() throws InterruptedException {
        Thread.sleep((long) (1000 + Math.random() * 11000));
        String xpath = "//a[contains(@href, '/profile/services')]";
        // нажатине на Мои сервисы
        driver.findElement(By.xpath(xpath)).click();

//        В хедере написано id
        String url = driver.findElement(By.xpath("//path")).getCssValue("background-image");
        Assert.assertEquals(url, "data:image/svg+xml;charset=utf-8,%3Csvg width='75' height='120' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 23.415V95h12.463V23.415H0zm24.792 0V95h15.76c19.775 0 33.99-12.772 33.99-36.153 0-26.368-14.112-35.432-32.96-35.432h-16.79zm14.73 61.903h-2.267V33.097h3.296c12.566 0 21.321 6.283 21.321 25.544 0 17.716-8.858 26.677-22.35 26.677z' fill='%23000'/%3E%3C/svg%3E)");
        Iterator<WebElement> allElements = driver.findElements(By.xpath("//li[contains(@class, 'sts__service')]")).iterator();
        Iterator<String> expected = Arrays.stream(new String[]{"Плюс", "Диск", "Афиша", "Маркет", "Избранное", "Карты"}).iterator();
        while (allElements.hasNext() && expected.hasNext()) {
            Assert.assertEquals(allElements.next().getText(), expected.next());
        }
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        Assert.assertEquals("ru",
                driver.findElement(By.xpath("//span[contains(@class, 'Link Link_pseudo Link_view_default')]")).getText());
        Assert.assertEquals("Справка",
                driver.findElement(By.xpath("//a[contains(@href, 'https://yandex.ru/support/passport/')]")).getText());
        Assert.assertEquals("Обратная связь",
                driver.findElement(By.xpath("//a[contains(@href, 'https://yandex.ru/support/passport/feedback.html')]")).getText());
        Assert.assertEquals("Удалить аккаунт",
                driver.findElement(By.xpath("//a[contains(@href, '/passport?mode=delete&origin=passport_profile')]")).getText());
        Assert.assertEquals("© 2001-2021, ",
                driver.findElement(By.xpath("//div[contains(@class, 'n-footer__rights')]")).getText());

    }

    @Test
    public void afishaTest1_5_2() throws InterruptedException {
        Thread.sleep((long) (1000 + Math.random() * 11000));
        String xpath = "//a[contains(@href, '/profile/services')]";
        // нажатине на Мои сервисы
        driver.findElement(By.xpath(xpath)).click();
        Thread.sleep((long) (1000 + Math.random() * 8000));
        driver.findElement(By.xpath("//span[contains(text(), 'Афиша')]")).click();
        Thread.sleep((long) (1000 + Math.random() * 4000));
        Assert.assertEquals("Афиша",
                driver.findElement(By.xpath("//a[contains(@href, 'https://afisha.yandex.ru') and (@class, 'd-link d-link_black')]")).getText());
        Assert.assertEquals("Найти развлечение",
                driver.findElement(By.xpath("//a[contains(@class, 'subscription__title d-link') and (@href, 'https://afisha.yandex.ru')]")).getText());
        Assert.assertEquals("На Афише есть кино, театры, концерты, выставки, детские праздники и всё остальное",
                driver.findElement(By.xpath("//div[contains(@data-t, 'afisha:start-desc1')]")).getText());
        Assert.assertEquals("Моя Афиша",
                driver.findElement(By.xpath("//a[contains(@class, 'subscription__title d-link') and (@href, 'https://afisha.yandex.ru/users/al-mukhametshina57')]")).getText());
        Assert.assertEquals("Все избранные события, купленные билеты и&nbsp;избранные места",
                driver.findElement(By.xpath("//div[contains(text(), 'Все избранные события, купленные билеты и&nbsp;избранные места')]")).getText());
    }

    @Test
    public void afishaTest1_5_3and4() throws InterruptedException {
        Thread.sleep((long) (1000 + Math.random() * 11000));
        String xpath = "//a[contains(@href, '/profile/services')]";
        // нажатине на Мои сервисы
        driver.findElement(By.xpath(xpath)).click();
        Thread.sleep((long) (1000 + Math.random() * 8000));
        driver.findElement(By.xpath("//span[contains(text(), 'Афиша')]")).click();
        Thread.sleep((long) (1000 + Math.random() * 2000));
        driver.findElement(By.xpath("//div[contains(@class, 'arrow arrow__right click-scale')]")).click();
        Assert.assertEquals("Концерт", driver.findElement(By.xpath("//div[contains(@class, 'afisha-card__type')]")).getText());
        Assert.assertEquals("Все события", driver.findElement(By.xpath("//div[contains(@class, 'arrow-link__text')]")).getText());
    }


}
